import os
def Tecla():
    input('\nPresione un tecla para volver al menu')
def Salir():
    print('El programa ha finalizado')
def menu():
    os.system('cls')
    print("a) Registrar productos")
    print("b) Mostrar Productos ")
    print("c) Mostrar productos cuyo stock se encuentre en el intervalo [desde, hasta]")
    print("d) Incrementar stock")
    print("e) Eliminar productos con stock 0")
    print("f) Salir")
    opcion=(input('Seleccione una opcion: '))
    while opcion!='a' and opcion!='b' and opcion!='c' and opcion!='d' and opcion!='e' and opcion!='f':
        opcion=(input('Seleccione una opcion(entre a y f): '))
    return opcion 

def eliminarProducto(diccionario):
    os.system('cls')
    i = 1
    while i <= len(diccionario):
        for clave, valor in diccionario.items():
            if (valor[2] == 0):
                print('se va a eliminar: ', diccionario[clave])
                del diccionario[clave]
                print('Eliminado exitosamente')
                break
        i+=1
    return (diccionario)

def incrementarStock(diccionario):
    os.system('cls')
    y = int(input('Ingrese un valor: '))
    x = int(input('Ingrese cantidad a incrementar de stocks: '))
    for clave, valor in diccionario.items():
        if (valor[2] < y):
            valor[2] = valor[2] + x
    print('Stock incrementado')
    

def mostrarDesdeHasta(diccionario):
    os.system('cls')
    desde = int(input('Desde stock: '))
    hasta = int(input('Hasta stock: '))
    for clave, valor in diccionario.items(): 
        if (valor[2] >= desde) and (valor[2] <=  hasta):
            print(clave,valor)

def mostrarProductos(diccionario):
    os.system('cls')
    print('.....LISTADO DE PRODUCTOS.....')
    for clave, valor in diccionario.items():
        print(clave,valor)

def validarPrecio():
    x = float(input('Precio: '))
    while not (x > 0) :
        x = float(input('Precio: '))
    return x

def validarStock():
    x = int(input('Stock: '))
    while not (x > -1) :
        x = int(input('Stock: '))
    return x

def CargarProductos():
    productos = {}
    print('***CARGAR PRODUCTOS***')
    res = 's'
    while (res =='s') or (res == 'S'):
        codigo = int(input('\nCodigo: '))
        if codigo not in productos :
            descripcion = input('Descripcion: ')
            precio = validarPrecio()
            stock = validarStock()
            productos[codigo] = [descripcion, precio, stock]
            print ('\nProducto cargado correctamente')
            res= input('Continuar cargando productos? s/n : ')
        else : 
            print ('El Producto ya existe')
    return productos


#PRINCIPAL
op = 0
productos={}
while op != 'f':
    op = menu()
    if op== 'a':
        os.system('cls')
        productos = CargarProductos()
        Tecla()
    elif op == 'b':
        os.system('cls')
        mostrarProductos(productos)
        Tecla()
    elif op == 'c':
        os.system('cls')
        mostrarDesdeHasta(productos)
        Tecla()
    elif op == 'd':
        os.system('cls')
        incrementarStock(productos)
        Tecla()
    elif op== 'e':
        os.system('cls')
        eliminarProducto(productos)
        Tecla()
    elif op=='f':
        Salir()